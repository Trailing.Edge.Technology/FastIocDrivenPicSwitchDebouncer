/*	Header File for Fast Switch Debounce DEMO.  A 'C' header file.

	Copyright (c) 2021-2022, Trailing Edge Technology.

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.  */


#ifndef APPLICATION_H
#define	APPLICATION_H

#ifdef	__cplusplus
extern "C" {
#endif

	#include	"SwitchHandler.h"		// Needed for SFR_T typedef.


//	Function Definitions for Functions referenced OUTSIDE of Application.c

//	Called by MCC's interrupt_manager.c (as modified for this project).
	void Timer0_InterruptHandler (void);

//	Called (back) by SwitchHandler.c
	void AppSwitchChanged (SFR_T oldValue, SFR_T newValue);
	void AppSwitchChangeEnqueue (SFR_T mask, SFR_T value);
	void AppDebounceTimerEnqueue (SFR_T mask);

//	Called by MCC's main.c (as modified for this project)
	void ApplicationMain (void);

#ifdef	__cplusplus
}
#endif

#endif	/* APPLICATION_H */

//	Copyright (c) 2021-2022, Trailing Edge Technology, See Statements at top of this file.
