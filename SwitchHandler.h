/*  Fast, IOC-Driven Switch Debouncer for a Single PIC Port.  C header file.

    Copyright (c) 2021-2022, Trailing Edge Technology.

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.  */


#ifndef SWITCHHANDLER_H
#define	SWITCHHANDLER_H

#ifdef	__cplusplus
extern "C" {
#endif

//	Requires	<xc.h>.
//	Requires	<stdint.h>	for XC8 versions BEFORE v2.35.

	
//	Create a typedef for an Unsigned Type the Same Width as the SFRs for this Architecture.
#ifdef __XC8
	typedef	uint8_t 	SFR_T;
#endif
#ifdef __XC16
	typedef	uint16_t	SFR_T;
#endif
#ifdef __XC32
	typedef	uint32_t	SFR_T;
#endif


//	#defines for Voltage Regulator Setting.  1 -> LowPower, SlowWake.  Not used for LF parts.
	#define SwitchVrSfr		VREGCONbits	// VoltageRegulator SFR.  Use: "SwitchVrSfr.
	#define SwitchVrBit		VREGPM		// VoltageRegulator Bit.        SwitchVrBit =
	#define SwitchVrVal		1			// VoltageRegulator Value.      SwitchVrVal;


//	#defines for IOC Interrupt Control.  Use IOCIE if Available, otherwise use GIE.
	#define SwitchIoceSfr	PIE0bits	// Interrupt Enable SFR.  Use: "SwitchIoceSFR.
	#define SwitchIoceBit	IOCIE		// Interrupt Enable Bit.        SwitchIoceBit = 0/1;"


//	#defines for SFR Names to Allow use of Different Ports.
	#define	SwitchPort		PORTC		// Port (Input).
	#define	SwitchIocf		IOCCF		// Changed Flags.
	#define	SwitchIocp		IOCCP		// Enable Bits for Rising Edge Detector.
	#define	SwitchIocn		IOCCN		// Enable Bits for Falling Edge Detector.


//	#defines for Switch Mask and Count.
	#define	SwitchMask		((SFR_T) 0x3F)	// Bits 0-5.
	#define SwitchCount		6				// Must Match Count of '1' Bits in Mask.


//	Assumed Initial Switch Bit Values.
	#define SwitchesAtStartup	SwitchMask	// ALL Switches Open (1 == Open).


//	#define for Switch Debounce Period.
//	Smallest Value that Reliably exceeds the Switches' longest Bounce Time is best.
	#define	SwitchDebounceMicroseconds	15000	// 15 ms.


//	Functions _Implemented_ by the SwitchHandler.c and referenced in other source files.
	void Switch_IOC_InterruptHandler (void);
	void SwitchInitialize (void);
	void SwitchChanged (SFR_T intrSwitchMask, SFR_T intrSwitchValue);
	void SwitchDebounceEnd (SFR_T switchMask);	// Do NOT Inline.


#ifdef	__cplusplus
}
#endif

#endif	/* SWITCHHANDLER_H */

//	Copyright (c) 2021-2022, Trailing Edge Technology, See Statements at top of this file.
