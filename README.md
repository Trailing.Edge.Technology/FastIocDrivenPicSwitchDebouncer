# FastIocDrivenPicSwitchDebouncer

## Name
A Fast, Low-Power, IOC-Driven Switch Debouncer for a Single PIC Port in C.

## Description
This algorithm uses the Interrupt (Request) Flag bits of the Interrupt-On-Change feature on most modern PIC
(and other) microcontrollers to 'Debounce' mechanical switches with very low power usage and minimal delay.

As supplied, this is an MPLabX Project using MPLabX v6.00 and XC8 v2.40.  It happens to use the PIC16F15324.
To use with other PIC microcontrollers, see "Customizing by Modifying SwitchHandler.h" in the documentation.

Likely usable with some minor changes on other microcontrollers which share the PIC family's IOC semantics.

## Further Information
See either SwitchDebounce.md or its derivative SwitchDebounce.pdf.

## Support
Visit the Microchip Forum Entry for this design at:
https://www.microchip.com/forums/FindPost/1212726 .

## Author
Glenn R. Peterson

## License
GPL 3.0

## Project status
Complete subject to feedback.
