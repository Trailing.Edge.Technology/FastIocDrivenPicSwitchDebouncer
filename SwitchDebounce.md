# A Fast, Low-Power, IOC-Driven Switch Debouncer for a Single PIC Port in C.

SwitchHandler.c implements a simple but subtle algorithm for debouncing
mechanical switches that is Interrupt-On-Change (IOC) driven.  Together
with an appropriate task manager (in a simple main program), this method
will result in minimal latency and can achieve very low power usage.

## Description

This algorithm uses the Interrupt (Request) Flag bits of the
Interrupt-On-Change feature on most modern PIC (and other) microcontrollers
to 'Debounce' mechanical switches with very low power usage and minimal
delay.

Although this description is fairly long due to the somewhat subtle algorithm,
**the switch-related code is only 17 executable statements**.  About three
times that many executable statements are required in Application.c for
support and power management, but most would be required for any real
application.

In the Interrupt Handler, an entry is made in a queue of switch changes
for processing at the task (not interrupt) level.  The main loop (an
example is provided in Application.c) must process the queue and that
will (indirectly) inform the application logic of the switch changes.

Switch changes are reported as soon as detected and processed, not at the
end of the 'Debounce Period' as is characteristic of some software switch
debouncers.  Waiting to the end of the Debouce Period to report switch
changes only adds latency unless filtering short changes is also an
objective.

The code is intended for a single port and supports as many switches as
can be connected to such a port.  Some ways to overcome this limit are
discussed below.

The SwitchHandler is unaware of the details of the **use** of the
Switches.  It merely reports changes in the Switch Pins using a
callback.  This partitions the problem cleanly.

Although written for a PIC16F15324, the SwitchHandler code should work
without modification on any PIC which shares the IOC semantics, including
those with wider Ports.  With minor modifications, it may work on non-PIC
microcontrollers which share the IOC semantics.

Changes needed for a specific use with a particular PIC microcontroller
should be confined to modifying #defines in SwitchHandler.h.  SwitchHandler.c
should not need modifications.  See section below.

It is assumed but not required that the switches connect the switch pins to
GND and the switch pins have their programmable "Weak Pullups" enabled.  This
is usually the most economical design.

## GPL 3 License for SwitchHandler.c/h and Application.c/h

SwitchHandler.c, SwitchHandler.h, Application.c, and Application.h are being
released under GPL 3.0.  This means anyone is free to use them and/or modify
them as long as the resultant software is also released under GPL 3.0.  This
is known as "copyleft".  Other licensing is possible, see the comments near
the top of SwitchHandler.c.

The MCC-generated files are licensed by Microchip under the terms shown in
each source and header file.


# The Algorithm

The algorithm uses the Positive and Negative Edge Detectors (IOCxP and
IOCxN) to detect switch changes on switch pins.  Using both detectors at
once makes the code simpler and faster.  [Earlier versions enabled only
one detector depending on the current state of the pin.  This had no
advantages and was replaced with this simpler version.]

The Interrupt Handler for Switch IOC Flags maintains the switch pin states
by accumulating XORs of the initial state and the sequence of (Interrupt
Request) **Flag** bits.  The Switch States are **always** computed from the
**Flag** bits; the **Port** bits are never used directly.

Since a given Flag bit is set only when the hardware or this software
detects a change from the currently known state, XORing the Flag bit(s)
with the **formerly** known state is guarenteed to produce the state
which caused the Flag bit(s) to be set: the **next** known state.

Debouncing is achieved by disabling **both** Edge Detectors for a
switch pin for the duration of the "Debounce Period" as declared in
SwitchHandler.h.  Changes made **during** the Debounce Period are
ignored which is the purpose of a Switch Debouncer.  A change sensed
at the **end** of the Debounce Period will cause the appropriate IR
Flag(s) to be set by this software and an Interrupt will enqueue a
switch change notice.

This is **not** a glitch filter.  Mechanical switches do not typically
cause glitches, so the need to filter short changes is limited to designs
where significant noise is present or as dictated by the application.  This
software will report a transient as two switch changes separated by the
Debounce Period.

IOC interrupts are disabled (by disabling both Edge Detectors) for switches
being Debounced.  This prevents the Switch State (as understood by the
Interrupt Handler) from being modified for those pins connected to Switches
being Debounced as the Flag bits will not be set by the hardware.

## Initialization

SwitchInitialize enables the Edge Detectors for each Switch Pin.  Occasionally
a switch bit is not in the expected initial state.  This will cause the
corresponding IOCxF bit to be set by this software.  This, in turn, will cause
an interrupt which will subsequently inform the Application of the unexpected
switch state(s) at startup.  SwitchInitialize is just a special invocation
of SwitchDebounceEnd.

## Switch IOC Interrupt Handler

When one or more IR Flags are set by an Edge Detector or this software,
an IOC interrupt will occur.  The Interrupt Handler disables both
Edge Detectors for each channged bit, clears the Flag(s), XORs the old
Switch States and the Flags to determine the currrent (new) Switch States, and
enqueues a SwitchChange for the non-interrupt code to dequeue and send to the
SwitchChanged function.  The Switch IOC Interrupt Handler acts as a
**serializer** because it **is** an Interrupt Handler.

## Switch Change Handling

The SwitchChanged function does three things: enqueues a DebounceEnd timer
event, informs the application via a callback (to uncouple the application
logic from switch handling), and saves the new state of the switches so
the application is given both the old and new values each time it's called.

## Debounce Period End

The SwitchDebounceEnd function is somewhat subtle.  It enables both Edge
Detectors (per switch pin being debounced in this request) and determines if
the switch is in the expected state and sets the IR Flag if it isn’t.  [This
is the **only** time the Port itself is accessed.]  The IR Flag is set IFF
the switch state undergoes a **net** change during the Debounce Period.  The
IR Flag will cause an IOC interrupt and the Interrupt Handler computes the new
Switch Bit(s) and the Values are passed to the Application via the SwitchChange
Queue.

A detailed examination of the sequence shows a small window where an Edge
Detector can set a Flag or Flags while in this function, but the usual case
is that it's done explicitly by this function if at all.  Note it is
**critical** that the IORing to the (IR) Flags is done **after** the Edge
Detector is Enabled - otherwise a pin change might be missed.

## Services Used by SwitchHandler.c

The SwitchHandler uses several facilities which must be provided by the
Application program:

- Enqueue a SwitchChange Description (from the Interrupt Handler).
- Enqueue a Debounce Timer (from the SwitchChange function).
- Process a SwitchChange (a callback to the Application).

### The SwitchChange Queue

The SwitchChangeQueue holds SwitchChanges detected by the Switch
IOC Interrupt Handler which have not yet been processed by the
Application.  SwitchChange entries are composed of two elements:

- a mask indicating which switches have changed, and
- the state of all of the switch bits as understood by the
Interrupt Handler at the time of the interrupt.

This queue is usually empty but must have space for 'SwitchCount'
entries.  The queue is drained by the application's main loop which
must pass the entries to the SwitchChange function.

### The DebounceTimer Queue

This queue's entries have a single element: a mask indicating which
switches have the same Debounce expiration.  Its size and occupancy
are the same as the SwitchChange Queue.

As implemented, all Switches have the same Debounce Period.  This
is not necessarily the case.  Supporting non-uniform Debounce Periods
would require changes in the implementation of the DebounceTimer
Queue (in Application.c in the Demonstration Project), but are
straight-forward.

## Actions of the Main Loop

In addition to supporting other aspects of the application, the main
loop must:
- drain the SwitchChange Queue expeditiously and pass the elements to the
SwitchChange function.  The entry should be removed from the queue before
SwitchChange is called with its contents.
- maintain the DebounceTimer Queue and pass the mask elements to
SwitchDebounceEnd when the Debounce time expires for the next entry(ies)
in the queue.


# Customizing by Modifying SwitchHandler.h

SwitchHandler.h has a number of #defines which should be inspected
and possibly changed so the resultant code matches the hardware
implementation.  This is in addition to using the project's properties
to set the MCU and using MCC (MPLab Code Configurator) or user-written
code to set the configuration, assign ports and pins, etc.  Using MCC is
**not** a requirement but is how the Demonstration Project was implemented.

## Setting the Voltage Regulator's Sleep State

Some 'F' parts allow the internal voltage regulator to be turned off during
SLEEP but this comes at the cost of longer wake-up time.  A set of #defines
in SwitchHandling.h control this behavior.  As supplied, the VREGCON.VREGPM
is Set during initialization to minimize power use as the Demonstration
program was developed using an 'F' part and response time was acceptable
even with the longer wakeup.

When using an 'LF' part, or an 'F' part that does not support VREGCON, simply
turn the 'SwitchVrVal' #define into a comment.  If used with an 'F' part
that **does** support VREGCON, either leave the 'SwitchVrVal' #define as a
'1' or change it to '0' if response time is more important than power usage.

## Controlling How Interrupts are Enabled and Disabled

In SwitchHandler.c, IOC Interrupt Control is done by clearing and
setting the bit "SwitchIoceSfr.SwitchIoceBit".  As supplied, this
is mapped to "PIE0bits.IOCIE" which is appropriate for the PIC16(L)F15324
(and many other PICs).  But other PICs have the IOCIE bit in other
SFRs.  For example, the PIC16(L)F1527 and many other PICs have the
IOCIE bit in the INTCON SFR.  Some older PICs may not support
IOCIE-specific control.  In the latter case mapping to INTCON.GIE
may be the finest-grained IOC interrupt control available - its use
is acceptable.

## Selecting the Port

As supplied, SwitchHandler.h specifies the SwitchBits are in Port
C.  This is generally appropriate for PICs in 14 to 20 pin packages,
but the implementation is Port-agnostic.  To select a different Port,
change the #defines for: SwitchPort, SwitchIocf, SwitchIocp,
and SwitchIocn.

## Setting the Port pins used for Switches

As supplied, SwitchHandler.h declares that the six (6) low-order bits of
the selected Port are switch bits.  That is, they are treated as switches and
handled by SwitchHandler.c.  Consequentially **'SwitchMask'** is defined as
**0x3F**.  This should be changed to match the actual switch pins used.  An
associated #define, **'SwitchCount'**, should be set to the number of '1'
bits in 'SwitchMask'.  'SwitchCount' should be used in the application to
test if the SwitchChange Queue and DebounceTimer Queue allocations are
sufficiently large - see Application.c.

## Setting the Expected Startup Switch Values

As supplied, SwitchHandler.h declares all switches pins are high (= 1) in
the definition of **'SwitchesAtStartup'**.  Adjust if needed.  As soon as
interrupts are enabled after the call to SwitchInitialize, the
application will be notified of a "SwitchChange" for any switch pin
that does not initially match 'SwitchesAtStartup'.

## Setting the Debounce Period

Mechanical switches vary greatly in their contact bounce properties.  Some
take as little as 1 millisecond to settle.  Others can take more than 25
milliseconds.  For the best responsiveness, specify the Debounce Period
as the smallest value that exceeds the longest bounce.  Quality mechanical
switches generally specify their contact bounce characteristics.  As supplied,
**'SwitchDebounceMicroseconds'** is declared as 15000 (15 milliseconds) and
that symbol is used in the sample application's DebounceTimer Queue handler. 
Note that a Debounce Period exceeding 25 milliseconds may be noticable by
human users.

## Special Function Register Datatype

SwitchHandler.h tests the Microchip compiler type to determine the width of
special function registers.  If this code is used with another toolset, it
will be necessary to modify the tests and definition.  SFR_T should be a
typedef set to an unsigned integer type with the same width as the special
function registers.

# Other Considerations

Initialization of the switch pins, using MCC or directly with user-written
code, should set them to digital inputs and enable the Weak Pullups if that
design is chosen.  **In no case** should Initialization enable any Edge
Detectors for any **switch pin** - SwitchInitilize will enable the appropriate
Edge Detectors.

The demonstration project overrides MCC's IOC handling in order to process
**all** the IOC Flag bits in parallel.

If the application uses IOC pins **not** associated with Switches, then
it must supply an IOC Interrupt Handler for them and arrange for it to
be called, see previous paragraph.

For minimal power consumption, unused peripherals should be powered-down
on those microcontrollers that support Peripheral Module Disable (PMD) or
its equivalent.  Intermittently-used peripherals should have their power
managed dynamically.

This software was built and tested with Microchip's **MPLabX v6.00** with
**XC8 v2.40**.  If using a version of XC8 before v2.35, a #include of
<stdint.h> will be needed, see SwitchHandler.h comment about same.

In the supplied project, the device configuration was done with **MCC**
**v5**.  When changing devices in an MCC project, the recommendation is
to create a new project, select the device, and re-configure the new
device.  Changing the device after MCC setup is not recommended and may
lead to errors.  **Using MCC is optional**, devices may be configured
using normal (user-written) code.

Note that some of the MCC-generated code was modified to improve
performance and clarity.  Specifically note the IOC changes result in
a **single** call to the IOC interrupt handler
(Switch_IOC_InterruptHandler).  Additionally, some overhead is bypassed,
note any conditionals using **__FAST_INTERRUPT_LINKAGE**.  For example,
interrupt_manager.c's **INTERRUPT_InterruptManager**.

The general convention is that global symbols are capitalized and local
symbols are not: **GblSym**, **locSym**.

The source code contains tabs and the tabstops are expected to be four (4)
characters apart (5, 9, 13, ...).  Conversion of tabs to spaces is a matter
of preference, but the distributed code will retain tabs and may not
compare as expected.

The source and text files follow the Windows convention for line separation: \<cr>\<lf>.

# Application.c Notes

Application.c is a fairly complete demonstration of the use of the Switch
Handler.  It includes the two Queues (SwitchChange and DebounceEnd), some
power management for Timer0 and Peripheral clocks, the main loop which
includes two **Tasks** (Dequeuing SwitchChange elements and Debounce Timing),
and the SLEEP logic.  The type of SLEEP (true SLEEP vs. IDLE) is maintained
by the DebounceQueue Timing code: Full SLEEP if Timer0 is not running and
IDLE if it is running.  Full SLEEP lowers power use as all clocks are off.

The simple task manager is controlled by a byte with two used bits for the
two tasks.  The AppSwitchChangeEnqueue code sets its bit when an element
is added by a call from the IOC Interrupt Handler.  It is carefully cleared
in the main loop when SwitchChange elements are Dequeued.  The TimeTick task
bit is set by the Timer0 Interrupt Handler and cleared when detected in the
main loop.

It is essential that the Queues are drained in a timely fashion by the main
loop.  Failure to service the queues could result in queue overflows.  In
particular, process both queues until empty within the Debounce Period.

The AppSwitchChange uses the switch states to control three (3) LEDs.  It is
somewhat atypical in that is does not use the previous switch states.  It
merely inspects the new switch states.

This software does not provide protection for pre-emptive multi-tasking;
it's designed for cooperative multi-tasking where tasks run to
completion.  Cooperative multi-tasking is appropriate for small and
medium complexity problems.  The **algorithm** can be used with pre-emptive
multi-tasking, but some synchronization mechanism would need to be used such
as Critical Sections, Task Switch Disable, ... .

Timer0 is Power-Managed.  When not needed (when the Debounce Queue is empty),
its PMD bit is set.  When the Debounce Queue has a new element added, the
Timer0 PMD bit is examined.  If the PMD bit is Set then: the PMD bit is
cleared (turning Timer0's Power On), Timer0 is initialized (when unpowered,
its state is lost), and finally Timer0 is Enabled.

In Application.c (unlike SwitchHandler.c), SFRs are referred to directly (no
#defines) because Application.c is specific to the application whereas
SwitchHandler.c is intended to be independent of application's specifics.

# Possible Enhancements

## To SwitchHandler

### Performance and Code Size

Three of the four (3 of 4) of the functions in SwitchHandler.c are called
just once making them ideal candidates for declaring them "inline".  This
has **not** been done here in order to keep all switch-related logic in
the 'C' file (SwitchHandler.c).  Do **not** "inline" SwitchDebounceEnd -
it has multiple callers and is moderately long.

### Consistent Response Time

Response time is defined as the interval from the switch change to the call
to the switch-change-handling application code at the task level
("AppSwitchChanged").  The demonstration program exhibits roughly constant
and low response time.  But a more complete Application.c could exhibit
longer and more varying response times as the number and execution time
of the tasks supported increase.

Using a simple multi-tasking main loop may not provide adequate switch
response if the Switch-related queues are checked once per loop.  To
improve responsiveness, checking the queues after each long-running
task will minimize latency.

For critical response behavior, the IOC Interrupt Handler could directly
perform some critical task - such having the interrupt handler respond
to an emergency button to perform an immediate motor stop.  Generally an
Interrupt Handler should service the interrupt and cause the main loop to
later respond to the interrupt cause.  Interrupt Handlers should be "short
and sweet".  But no rule is absolute and engineering must handle competing
priorities.

### Handling Multiple Ports

The current code supports a single Port.  This code uses the SFRs
for a Port and its IOC Registers as known at compile time (no
indirections).  This is more efficient and reduces concerns about
atomicity.

Although using more than six (6) switches is not common (and eight (8),
16, or 32 could be supported with a single Port on other PICs), some
applications might need to support switches connected to multiple
Ports.  It's certainly possible to use this **algorithm** for more than
one Port, but not in its current form.  Some possible ways to support
multiple Ports are:

- Brute-force cloning of the four functions, their static variables,
and their Port-specific #defines.  This will create maintenance
'opportunities'.
- Adding a context structure containing the statics and pointers to
the SFRs.  It is essential the IOCxF manipulation be done atomically
(with single instructions) so the generated code would have to be verified.

Both approaches would require changes to the underlying services
(queues, main loop, high-level interrupt processing, ...).

## To Application.c

Application.c is a modest implementation.  It has no real purpose beyond
demonstrating the use of SwitchHandler.c, performing very simple cooperative
multi-tasking, and showing some rudimentary power management.  To be useful,
it must be expanded to support the requirements of the application being
implemented.

The DebounceQueue is a very specialized Timer Queue: fixed time deltas
and a simple entry.  In a sophisticated application, a general purpose
Timer Queue may be present.  In that case, a DebounceQueue Entry **might**
be just one type of TimerQueue Entry.

In a real application with a cooperative multi-tasker, the longest
task execution length must be minimized if switch response time is
important.  That's because switch processing (by the application logic)
is done at the Task level.  As a rule-of-thumb, the maximum time the
switch processing can be postponed is about 25 milliseconds before a
human with good reaction times will perceive the delay.

As mentioned under "Consistent Response Times", multiple checks of the
Switch-related queues within the main loop might be appropriate.


# Minimizing Power Use

This code was originally developed for a simple but "Always On" system
powered by a vehicle's starting battery - so minimal power use was
important.  Days, weeks, or even months could go by with no activity:
the MCU SLEEPing and its clocks stopped.

Self-discharge of an automotive starting battery at cool temperatures is
about 3 milliAmps.  The **total** of all "Always-On" loads should be less
than 1/10 of that to avoid significant reduction of the battery endurance:
300 microAmps.  The Demonstration application consumes less than
**0.5 microAmps** at Vdd = 5v measured at room temperature when idle.  Modern,
low-quiescent-current voltage regulators (such as TI's TPS709 series) consume
less than 2 microAmps of ground current.  So a total of less than 3 microAmps
will power this application when idle.  That will not significantly lower the
endurance of even a fairly small battery.

MCC is used to initially turn power off to all peripherals except the IOC
peripheral.  This includes Timer0 as its power is dynamically managed;
Timer0's power will be applied as needed.

As part of the static configuration, close attention should be paid to
the power use implied by each choice.  In particular, the Brown-out
Reset circuit consumes much more power than the more modern Low-Power
Brown-out Reset circuit.  In the supplied project, BOREN is set to 00
(Disable) and ~LPBOREN is set to 0 (Enable).

Other PIC MCUs have lower idle current, some as low as 50 nanoAmps
typical.  Designs for extremely low power need to use "Best Practices"
for board design and cleaning (especially of solder flux) in addition
to good software power management.  This is particularly important when
using small batteries for power.


# Packaging

The project is provided as two source files, two header files, two
documentation files, and a ".zip" file containing the MPLabX project
files (including MCC control and generated source and header files):

- SwitchHandler.c and SwitchHandler.h
- Application.c and Application.h
- SwitchDebounce.md and SwitchDebounce.pdf
- DebounceDemo.zip

The project's name is DebounceDemo and by MPLabX convention is is stored
in a directory named "DebounceDemo.X".

On GitLab, the project can be found at:
https://gitlab.com/Trailing.Edge.Technology/FastIocDrivenPicSwitchDebouncer.
